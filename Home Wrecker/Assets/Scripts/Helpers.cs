﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

internal static class Helpers {
    public static Vector2 VectorFromDegrees(float degrees)
    {
        var radians = Mathf.Deg2Rad * degrees;

        return new Vector2(Mathf.Cos(radians), Mathf.Sin(radians));
    }
}
