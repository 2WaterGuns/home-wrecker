﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileScript : MonoBehaviour {

    public PlayerScript OriginatingPlayerScript;

    internal float Damage;
    internal float KnockbackMag;
    /// <summary> 0 = full horizontal away from collision, 90 = full vertical </summary>
    internal float KnockbackAngle;

    internal Vector2? StartingPosition;
    internal float DistanceToDespawn;

    private Rigidbody2D myBody;
    private SpriteRenderer myRenderer;

    internal bool faceLeft;

    // Use this for initialization
    void Start () {
        myBody = GetComponent<Rigidbody2D>();
        myRenderer = GetComponent<SpriteRenderer>();

        myRenderer.flipX = faceLeft;
    }
	
	// Update is called once per frame
	void Update () {
        if (!StartingPosition.HasValue)
        {
            StartingPosition = transform.position;
        }

        myRenderer.flipX = faceLeft;

        var distanceFromStart = Vector2.Distance(StartingPosition.Value, transform.position);
        if (distanceFromStart >= DistanceToDespawn)
        {
            Destroy(gameObject);
        }
	}

    void OnTriggerEnter2D(Collider2D collision)
    {
        var incomingObject = collision.gameObject;
        if (incomingObject.CompareTag("Player"))
        {
            var incomingPlayer = incomingObject.GetComponent<PlayerScript>();
            if (incomingPlayer.PlayerName == OriginatingPlayerScript.PlayerName)
            {
                return;
            }

            incomingPlayer.TakeDamage(myBody.position.x, Damage, KnockbackMag, KnockbackAngle);

            Damage = 0f;
            Destroy(gameObject);
        }
    }
}
