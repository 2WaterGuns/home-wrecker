﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClickDetectorScript : MonoBehaviour {

    public UnityEngine.Events.UnityEvent OnClick;

	// Update is called once per frame
	void Update () {
		if (Input.GetMouseButton(0))
        {
            OnClick.Invoke();
        }
	}
}
