﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerScript : MonoBehaviour {

    public PlayerScript OtherPlayerScript;

    public string PlayerName;

    public float Health;
    public Slider HealthSlider;

    public float JumpForce;

    public float MoveVelocity;
    public float SmoothTime;
    Vector2 currentAccel;
    public float MaxAccel;

    public GameObject ProjectilePrefab;
    public float ProjectileVelocity;
    public float ProjectileDelay;
    public float ProjectileDamage;
    public float ProjectileKnockbackMag;
    public float ProjectileKnockbackAngle;
    public float ProjectileDistance;

    private Animator myAnimator;
    private Rigidbody2D myBody;
    private SpriteRenderer myRenderer;

    public bool UseSecondaryColor = false;

    private enum PlayerState
    {
        Normal,
        Shoot,
        Hit
    }
    private PlayerState currentState;
    private bool hasJumpedButNotLeftGround = false;

    private float inputHorizontal;
    private bool inputJump;

    // Use this for initialization
    void Start ()
    {
        myAnimator = GetComponent<Animator>();
        myBody = GetComponent<Rigidbody2D>();
        myRenderer = GetComponent<SpriteRenderer>();

        currentState = PlayerState.Normal;
	}

    private bool IsTouchingGround
    {
        get { return myAnimator.GetBool("IsTouchingGround"); }
        set { myAnimator.SetBool("IsTouchingGround", value); }
    }
    
    private bool IsFacingLeft
    {
        get { return myRenderer.flipX; }
        set { myRenderer.flipX = value; }
    }
    
    // Update is called once per frame
    void Update() {
        if (UseSecondaryColor)
        {
            myRenderer.color = Color.grey;
        }

        // Poll input
        inputHorizontal = Input.GetAxisRaw(PlayerName + "_Horizontal");
        inputJump = Input.GetButtonDown(PlayerName + "_Jump");
        var inputShoot = Input.GetButtonDown(PlayerName + "_Shoot");

        if (currentState != PlayerState.Normal)
        {
            Update_WaitForAnimationToFinish();
        }
        else if (inputShoot)
        {
            currentState = PlayerState.Shoot;
            myAnimator.SetTrigger("ShootStart");

            StartCoroutine(Coroutine_Shoot(
                delay: ProjectileDelay,
                velocity: ProjectileVelocity,
                showObject: true,
                objectScale: 1.0f,
                damage: ProjectileDamage,
                knockbackMag: ProjectileKnockbackMag,
                knockbackAngle: ProjectileKnockbackAngle,
                distanceToDespawn: ProjectileDistance));
        }
    }

    void Update_WaitForAnimationToFinish()
    {
        var currentAnimState = myAnimator.GetCurrentAnimatorStateInfo(0);
        if (currentAnimState.IsTag("Normal"))
        {
            currentState = PlayerState.Normal;
        }
    }

    private void FixedUpdate()
    {
        switch (currentState)
        {
            case PlayerState.Normal:
                FixedUpdate_NormalState();
                break;
            default:
                break;
        }

        // Set animation params
        myAnimator.SetFloat("AbsoluteHorizontalSpeed", Mathf.Abs(myBody.velocity.x));
    }
    
    void FixedUpdate_NormalState()
    {
        // Flip if needed (doesn't apply when attacking or being hit)
        IsFacingLeft = OtherPlayerScript.transform.position.x < this.transform.position.x;
        
        // Move
        var targetVelocity = new Vector2(inputHorizontal * MoveVelocity, myBody.velocity.y);
        myBody.velocity = Vector2.SmoothDamp(myBody.velocity, targetVelocity, ref currentAccel, SmoothTime, MaxAccel, Time.fixedDeltaTime);

        // Add jump force
        if (inputJump && IsTouchingGround && !hasJumpedButNotLeftGround)
        {
            hasJumpedButNotLeftGround = true;
            myBody.AddForce(new Vector2(0, JumpForce));
        }
    }

    IEnumerator Coroutine_Shoot(float delay, float velocity, bool showObject, float objectScale, float damage, float knockbackMag, float knockbackAngle, float distanceToDespawn)
    {
        yield return new WaitForSeconds(delay);
        
        var projectile = Instantiate(ProjectilePrefab, myRenderer.bounds.center, Quaternion.identity);
        
        var direction = IsFacingLeft ? Vector2.left : Vector2.right;
        projectile.GetComponent<Rigidbody2D>().velocity = direction * velocity;

        if (!showObject)
        {
            var spriteRenderer = projectile.GetComponent<SpriteRenderer>();
            spriteRenderer.color = new Color(0f, 0f, 0f, 0f);
        }

        projectile.transform.localScale = new Vector3(objectScale, objectScale, 1.0f);

        var projectileScript = projectile.GetComponent<ProjectileScript>();
        projectileScript.OriginatingPlayerScript = this;
        projectileScript.Damage = damage;
        projectileScript.KnockbackMag = knockbackMag;
        projectileScript.KnockbackAngle = knockbackAngle;
        projectileScript.DistanceToDespawn = distanceToDespawn;
        projectileScript.faceLeft = IsFacingLeft;
    }

    void OnCollisionEnter2D(Collision2D collision) {
        if (collision.gameObject.CompareTag("Ground"))
        {
            IsTouchingGround = true;
        }
    }

    void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Ground"))
        {
            IsTouchingGround = false;
            hasJumpedButNotLeftGround = false;
        }
    }

    internal void TakeDamage(float strikeX, float amount, float knockbackMagnitude, float knockbackAngleDegrees)
    {
        Health -= amount;
        HealthSlider.value = Health;

        var hitLeft = strikeX < myBody.position.x;
        var actualDegrees = hitLeft ? knockbackAngleDegrees : 180 - knockbackAngleDegrees;
        var knockback = Helpers.VectorFromDegrees(actualDegrees) * knockbackMagnitude;
        myBody.AddForce(knockback);

        currentState = PlayerState.Hit;
    }

}
