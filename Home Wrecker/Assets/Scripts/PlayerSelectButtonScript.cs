﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSelectButtonScript : MonoBehaviour {

    public int CharacterIndex;

    public UnityEngine.Events.UnityEvent OnAllFilled;
    
    public void OnClick()
    {
        HackyStaticState.PlayerCharacterNumbers[HackyStaticState.NextPlayerToSelect] = CharacterIndex;
        HackyStaticState.NextPlayerToSelect++;

        if (HackyStaticState.NextPlayerToSelect >= 2)
        {
            OnAllFilled.Invoke();
        }
    }
}
