﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class GameSetDetectorScript : MonoBehaviour {

    public PlayerScript[] Players = new PlayerScript[0];

    public UnityEngine.Events.UnityEvent OnGameSet;

    // Update is called once per frame
    void Update () {
        if (Players.Length == 0) { return; }

        var survivors = Players.Where(p => p.Health > 0);
        switch (survivors.Count())
        {
            case 0:
                HackyStaticState.Winner = null;
                OnGameSet.Invoke();
                break;
            case 1:
                HackyStaticState.Winner = survivors.Single().PlayerName;
                OnGameSet.Invoke();
                break;
        }
	}
}
