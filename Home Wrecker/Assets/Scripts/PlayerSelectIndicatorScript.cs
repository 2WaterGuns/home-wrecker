﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSelectIndicatorScript : MonoBehaviour {

    private UnityEngine.UI.Text myText;

	// Use this for initialization
	void Start () {
        myText = GetComponent<UnityEngine.UI.Text>();
	}
	
	// Update is called once per frame
	void Update () {
        myText.text = "Player " + (HackyStaticState.NextPlayerToSelect + 1) + ",";
	}
}
