﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThanksScript : MonoBehaviour {

    private UnityEngine.UI.Text myText;

	// Use this for initialization
	void Start () {
        myText = GetComponent<UnityEngine.UI.Text>();
	}
	
	// Update is called once per frame
	void Update () {
        var toShow = "";
        toShow += "GAME SET!";
        toShow += "\n\n";
        if (HackyStaticState.Winner == null)
        {
            toShow += "No Contest";
        }
        else
        {
            toShow += "This game's winner is... " + HackyStaticState.Winner + "!";
        }
        toShow += "\n\n";
        toShow += "Thanks for Playing!";
        myText.text = toShow;
	}
}
