﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSpawnerScript : MonoBehaviour {

    public GameSetDetectorScript GameSetDetector;

    public GameObject[] Characters;
    public UnityEngine.UI.Slider[] HealthSliders;
    public float OffsetFromCenter;

    private bool hasRun;

	// Use this for initialization
	void Start () {
        hasRun = false;
	}
	
	// Update is called once per frame
	void Update () {
        if (hasRun) { return; }

        var p1Char = Characters[HackyStaticState.PlayerCharacterNumbers[0].Value];
        var p2Char = Characters[HackyStaticState.PlayerCharacterNumbers[1].Value];

        var p1 = CreatePlayerCharacter(1, p1Char);
        var p2 = CreatePlayerCharacter(2, p2Char);

        p1.OtherPlayerScript = p2;
        p2.OtherPlayerScript = p1;
        
        if (p1Char == p2Char)
        {
            p2.UseSecondaryColor = true;
        }

        GameSetDetector.Players = new PlayerScript[] { p1, p2 };

        hasRun = true;
        Destroy(this.gameObject);
	}

    private PlayerScript CreatePlayerCharacter(int playerNumber, GameObject character)
    {
        var spawnLocation = this.transform.position;
        spawnLocation.x = playerNumber == 1 ? -OffsetFromCenter : OffsetFromCenter;

        var obj = Instantiate(character, spawnLocation, Quaternion.identity);
        var playerScript = obj.GetComponent<PlayerScript>();

        playerScript.PlayerName = "P" + playerNumber;
        playerScript.HealthSlider = HealthSliders[playerNumber - 1];

        return playerScript;
    }
}
